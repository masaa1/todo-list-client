import {
    ADD_TODO,
    REMOVE_TODO,
    TOGGLE_TODO,
    EDIT_TODO,
    ADD_TODO_LIST,
    REMOVE_TODO_LIST,
    SHOW_TODO_LIST,
    UPDATE_FINISH_DATE,
    REMOVE_FINISH_DATE
} from './Types';
import uuid from 'uuid/v4';

const createIdActionCreator = type => id => ({type, id});

export const addTodo = (listName, task, id = uuid(), listId) => ({
    type: ADD_TODO,
    id,
    task,
    listName,
    listId
});

export const removeTodo = createIdActionCreator(REMOVE_TODO);

export const toggleTodo = createIdActionCreator(TOGGLE_TODO);

export const editTodo = (task, id) => ({
    type: EDIT_TODO,
    id,
    task
});

export const addTodoList = (newListName) => ({
    type: ADD_TODO_LIST,
    newListName
});

export const removeTodoList = (listToRemove) => ({
    type: REMOVE_TODO_LIST,
    listToRemove
});

export const showTodoList = (listToShow) => ({
    type: SHOW_TODO_LIST,
    listToShow
});

export const updateFinishDate = (finishDate, id) => ({
    type: UPDATE_FINISH_DATE,
    finishDate,
    id
});

export const removeFinishDate = createIdActionCreator(REMOVE_FINISH_DATE);