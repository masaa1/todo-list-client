import * as syncActions from './SyncCreators';

export default class AsyncCreators {
    constructor(communicator) {
        this.communicator = communicator;
    }

    addTodo = (listName, listId, task) => async dispatch => {
        let response = await this.communicator.addTodo({
            listId,
            task,
        });
        handle(response, dispatch, () => syncActions.addTodo(listName, task, response.id, listId));
    };

    removeTodo = (id) => async (dispatch) => {
        const response = await this.communicator.deleteTodo(id);
        handle(response, dispatch, () => syncActions.removeTodo(id));
    };

    toggleTodo = (id) => async (dispatch) => {
        const response = await this.communicator.toggleTodo(id);
        handle(response, dispatch, () => syncActions.toggleTodo(id));
    }

    editTodo = (task, id, listId) => async (dispatch) => {
        const response = await this.communicator.editTodoTask({task, listId}, id);
        handle(response, dispatch, () => syncActions.editTodo(task, id));
    }


    addTodoList = (newListName) => async (dispatch) => {
        const response = await this.communicator.addList(newListName);
        handle(response, dispatch, () => syncActions.addTodoList(response));

    }

    removeTodoList = (listToRemove) => async dispatch => {
        const response = await this.communicator.deleteList(listToRemove.id);
        handle(response, dispatch, () => syncActions.removeTodoList(listToRemove.id));
    }


    showTodoList = (listToShow) => syncActions.showTodoList(listToShow);

    updateFinishDate = (finishDate, id) => async dispatch => {
        const response = await this.communicator.updateFinishDate(finishDate, id);
        handle(response, dispatch, () => syncActions.updateFinishDate(finishDate, id));
    }


    removeFinishDate = (id) => async dispatch => {
        const response = await this.communicator.removeFinishDate(id);
        handle(response, dispatch, () => syncActions.removeFinishDate(id));
    }
};

const handle = (response, dispatch, action) => {
    if (response) {
        dispatch(action());
    }
}
