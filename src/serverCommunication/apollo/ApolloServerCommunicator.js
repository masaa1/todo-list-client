// necessary for tests to run
import 'cross-fetch/polyfill';
import "regenerator-runtime/runtime.js";
//
import { ApolloClient, gql } from 'apollo-boost';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { serverURL } from '../../constants/urls';
import ListInitialize from './Models/List/initialize';
import TodoInitialize from './Models/Todo/initialize';

const setUpApolloServerCommunicator = () => {
    const client = new ApolloClient({
        link: new HttpLink({ uri: serverURL + '/graphql' }),
        cache: new InMemoryCache()
    });        

    return {
        ...TodoInitialize(client),
        ...ListInitialize(client)
    }
}

export default setUpApolloServerCommunicator();
