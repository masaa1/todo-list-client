import {exportQuery} from '../helpers';

const queries = {
    getTodo: exportQuery(`
        query todo($id: ID!) {
            Todo(id: $id) {
                _id
                task
                finishDate
                completed
                listName
            }
        }
    `, 'Todo'),
    getAllTodos: exportQuery(`
        query todos {
            Todos {
                _id
                task
                finishDate
                completed
                listName
            }
        }
    `, 'Todos'),
    addTodo: exportQuery(`
        mutation AddTodo($todo : TodoInput!) {
            add_todo(todo : $todo)
        }
    `, 'add_todo'),
    editTodoTask: exportQuery(`
        mutation EditTodoTask($text: String!, $id:ID!) {
            edit_todo_task(text: $text, id: $id)
        }
    `, 'edit_todo_task'),
    deleteTodo: exportQuery(`
        mutation DeleteTodo($id : ID!) {
            delete_todo(id: $id)
        }
    `, 'delete_todo'),
    updateFinishDate: exportQuery(`
        mutation updateFinishDate($finishDate : String!, $id: ID!) {
            add_finish_date(finishDate: $finishDate, id: $id)
        }
    `, 'add_finish_date'),
    removeFinishDate: exportQuery(`
        mutation RemoveFinishDate($id: ID!){
            remove_finish_date(id: $id)
        }
    `, 'remove_finish_date'),
    toggleTodo: exportQuery(`
        mutation ToggleTodo($id: ID!) {
            toggle_todo(id: $id)
        }
    `, 'toggle_todo')
}

export default queries;