import queries from './queries';
import {CommunicationHelpers} from '../helpers';

const initialize = (client) => {
    let helpers = new CommunicationHelpers(client);
    return {
        getLists: async () => (
            await helpers.fetchQuery(queries.getLists)
        ),
        addList: async (listName) => (
            await helpers.fetchMutation(queries.addList, { listName })
        ),
        deleteList: async (listName) => (
            await helpers.fetchMutation(queries.deleteList, { listName })
        )
    }
}

export default initialize;