// necessary for tests to run
import 'cross-fetch/polyfill';
import "regenerator-runtime/runtime.js";
import { serverURL } from '../../constants/urls';
import RestFetcher from './RestFetcher';

const setUpApolloServerCommunicator = () => {
    const client = {
        link: serverURL,
    };        

    return {
        ...RestFetcher(client)
    }
}

export default setUpApolloServerCommunicator();
