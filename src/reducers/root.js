import shownListName from './shownListName';
import listNames from './listNames';
import todos from './todos';
import {combineReducers} from 'redux';

export default combineReducers({shownListName, listNames, todos})