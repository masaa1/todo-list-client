import {
  ADD_TODO_LIST,
  REMOVE_TODO_LIST,
  SET_STATE
} from '../actions/Types';
import initialState from '../constants/initialState';

const reducer = (state = initialState.listNames, action) => {
  switch (action.type) {
    case ADD_TODO_LIST: 
      return [...state, action.newListName];
    case REMOVE_TODO_LIST:
      return state.filter((listName) => listName.id !== action.listToRemove)
    case SET_STATE:
      return action.state.listsNames
    default:
      return state;
  }
};

export default reducer;
