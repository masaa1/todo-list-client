import {
  ADD_TODO,
  EDIT_TODO,
  REMOVE_FINISH_DATE,
  REMOVE_TODO,
  REMOVE_TODO_LIST,
  SET_STATE,
  SHOW_TODO_LIST,
  TOGGLE_TODO,
  UPDATE_FINISH_DATE
} from '../actions/Types';
import initialState from '../constants/initialState';

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TODO:
      return [...state, {...action, finishDate : 'default', completed: false, shown : true}]
    case REMOVE_TODO:
      return state.filter(todo => todo.id !== action.id)
    case TOGGLE_TODO:
      return state.map(todo =>
          todo.id === action.id ? { ...todo, completed: !todo.completed } : todo
        )
    case EDIT_TODO:
      return state.map(todo =>
          todo.id === action.id ? { ...todo, task: action.task } : todo
        )
    case SHOW_TODO_LIST:
      return state.map((todo =>
        todo.listId === action.listToShow.id ?
          {...todo, shown : true} : {...todo, shown : false}
        ))
    case REMOVE_TODO_LIST:
        return state.filter((todo) => todo.listId !== action.listToRemove);
    case UPDATE_FINISH_DATE:
        return state.map(todo =>
            todo.id === action.id ? { ...todo, finishDate: action.finishDate} : todo)
    case REMOVE_FINISH_DATE:
      return state.map(todo =>
          todo.id === action.id ? { ...todo, finishDate: '' } : todo
        )
    case SET_STATE:
      return action.state.todos
    default:
      return state;
  }
};

export default reducer;
