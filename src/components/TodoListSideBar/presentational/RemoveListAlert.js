import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function RemoveListAlert({todoListToRemove, removeTodoList, open, setOpen}) {
  const handleNo = () => {
    setOpen(false);
  };

  const handleYes = () => {
    removeTodoList();
    setOpen(false);
  }

  return (
    <div>
      <Dialog
        open={open}
        onClose={() => setOpen(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Remove"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Are you sure you want to delete the todo list : {todoListToRemove}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button data-test='noBut' onClick={handleNo} color="primary">
            No
          </Button>
          <Button data-test='yesBut' onClick={handleYes} color="primary" autoFocus>
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}