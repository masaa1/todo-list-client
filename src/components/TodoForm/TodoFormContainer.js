import { connect } from 'react-redux'
import TodoForm from './TodoForm';

const mapDispatchToProps = (dispatch, {actions}) => ({
    addTodo: (currentListName, value) => dispatch(actions.addTodo(currentListName.name, currentListName.id, value))
});

const mapStateToProps = state => ({
    currentListName: state.shownListName,
})

export default connect(mapStateToProps, mapDispatchToProps)(TodoForm);