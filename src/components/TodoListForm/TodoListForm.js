import React from 'react';
import useInputState from '../../hooks/useInputState';
import useStyles from '../../styles/TodoListFormStyles';

function TodoListForm({addTodoList}) {
  const classes = useStyles();
  const [value, handleChange, clearValue] = useInputState('');

  return (
    <form data-test='form'
      onSubmit={e => {
        e.preventDefault();
        addTodoList(value);
        clearValue();
      }}
      className={classes.TodoForm}
    >
      <input data-test='input'
        placeholder="Add your task list here..."
        value={value}
        onChange={handleChange}
        className={classes.input}
      />
    </form>
  );
}

export default TodoListForm;
