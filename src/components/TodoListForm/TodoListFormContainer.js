import { connect } from 'react-redux'
import TodoListForm from './TodoListForm';

const mapDispatchToProps = (dispatch, {actions}) => ({
    addTodoList: newListName => dispatch(actions.addTodoList(newListName))
});

export default connect(null, mapDispatchToProps)(TodoListForm);